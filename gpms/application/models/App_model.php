<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class App_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    //returns system all users
    public function get_system_users()
    {
        return $this->db->get('auth')->result();
    }
    //returns new users
    public function get_new_users()
    {
        return $this->db->where('stat_conf',0)->get('auth')->result();
    }
    //returns user details
    public function user_info($user_id)
    {
        return $this->db->where('user_id',$user_id)->get('auth')->result();
    }
    //return projects which have not yet been invoiced
    public function get_invoicing_projects()
    {
        return $this->db->order_by('project_id','DESC')
            ->group_start()
                ->where('status','closed')
                ->where('invoiced',0)
            ->group_end()
        ->get('projects')->result();
    }
    //returns all projects of the specified category
    public function get_projects($project_category)
    {
        if ($project_category == 'open') {
            return $this->db->order_by('project_id','DESC')->where('status','open')->get('projects')->result();
        }
        if ($project_category == 'closed') {
            return $this->db->order_by('project_id','DESC')->where('status','closed')->get('projects')->result();
        }
        if ($project_category == 'new') {
            return $this->db->order_by('project_id','DESC')->where('status','new')->get('projects')->result();
        }
    }
    //returns user projects of specified category
    public function get_user_projects($project_category)
    {
        if ($project_category == 'new') {
            return $this->db->order_by('project_id','DESC')
                ->group_start()
                    ->where('client',$this->session->userdata('user_id'))
                    ->or_group_start()
                        ->where('manager',$this->session->userdata('user_id'))
                    ->group_end()
                ->group_end()
                ->where('status','new')
            ->get('projects')->result();
        }
        if ($project_category == 'open') {
            return $this->db->order_by('project_id','DESC')
                ->group_start()
                    ->where('client',$this->session->userdata('user_id'))
                    ->or_group_start()
                        ->where('manager',$this->session->userdata('user_id'))
                    ->group_end()
                ->group_end()
                ->where('status','open')
            ->get('projects')->result();
        } else {
            return $this->db->order_by('project_id','DESC')
                ->group_start()
                    ->where('client',$this->session->userdata('user_id'))
                    ->or_group_start()
                        ->where('manager',$this->session->userdata('user_id'))
                    ->group_end()
                ->group_end()
                ->where('status','closed')
            ->get('projects')->result();
        }
    }
    //removes specified project from the database
    public function delete_project($project_id)
    {
        return $this->db->where('project_id',$project_id)->update('projects',array('delete' => 1));
    }
    //Adds project to the database
    public function new_projects()
    {
        return $this->db->where('status','new')->get('projects')->result();
    }
    //returns project details
    public function project_info($project_id)
    {
        return $this->db->where('project_id',$project_id)->get('projects')->result();
    }
    //send message
    public function send_message($data)
    {
        return $this->db->insert('messages',$data);
    }
    //returns messages of specified category
    public function fetch_messages($message_category)
    {
        if ($message_category == 'inbox') {
            return $this->db->order_by('message_id','DESC')->where('user_to',$this->session->userdata('user_id'))->get('messages')->result();
        } elseif ($message_category == 'sent') {
            return $this->db->order_by('message_id','DESC')->where('user_from',$this->session->userdata('user_id'))->get('messages')->result();
        } else {
            return $this->db->order_by('message_id','DESC')->where('draft',1)->get('messages')->result();
        }
    }
    //returns message details
    public function get_message($msg_id)
    {
        return $this->db->where('message_id',$msg_id)->get('messages')->result();
    }
    //delet message from the database
    public function trash($msg_id)
    {
        return $this->db->where('message_id',$msg_id)->delete('messages');
    }
    //save message to draft
    public function draft($msg_id)
    {
        return $this->db->where('message_id',$msg_id)->update('messages',array('draft' => 1));
    }
    //returns user details
    public function get_user($user_id)
    {
        return $this->db->where('user_id',$user_id)->get('auth')->result();
    }
    //delete user from database
    public function delete_user($user_id)
    {
        return $this->db->where('user_id',$user_id)->delete('auth');
    }
    //returns users of specified user group
    public function get_users($role)
    {
        if ($role == 'staff') {
            return $this->db->select('*')->from('auth')
                ->group_start()
                    ->where('role','staff')
                    ->or_group_start()
                        ->where('role','admin')
                    ->group_end()
                ->group_end()
            ->get()->result();
        } else {
            return $this->db->where('role',$role)->get('auth')->result();
        }
    }
    //returns currencies
    public function currency()
    {
        return $this->db->get('currency')->result();
    }
    //add new project
    public function new_project($data)
    {
        return $this->db->insert('projects',$data);
    }
    //returns project conversation
    public function get_conversation($get)
    {
        if ($get['show'] == 'all') {
            return $this->db->order_by('id','DESC')->where('project_id',$get['project_id'])->get('project_conversations')->result();
        } else {
            return $this->db->order_by('id','DESC')->limit(20)->where('project_id',$get['project_id'])->get('project_conversations')->result();
        }
    }
    //add project to the database
    public function post_project_chat($data)
    {
        return $this->db->insert('project_conversations',$data);
    }
    //edit project details
    public function edit_project($data)
    {
        return $this->db->where('project_id',$data['project_id'])->update('projects',$data['edit']);
    }
    //returns countries
    public function get_countries()
    {
        return $this->db->get('countries')->result();
    }
    //returns invoices
    public function get_invoices($category)
    {
        return $this->db->where('status',$category)->get('invoices')->result();
    }
    //delete invoice from database
    public function delete_invoice($invoice_ref)
    {
        return $this->db->where('ref_no',$invoice_ref)->delete('invoices');
    }
    //returns invoice items
    public function get_invoice_items($ref_no)
    {
        return $this->db->where('invoice_ref',$ref_no)->get('invoice_items')->result();
    }
    //returns invoice details
    public function get_invoice_info($ref_no)
    {
        return $this->db->where('ref_no',$ref_no)->get('invoices')->result();
    }
    //returns payments
    public function get_payments()
    {
        return $this->db->get('payments')->result();
    }
    //returns the cost of specified project
    public function get_cost($project_id)
    {
        return $this->db->where('project_id',$project_id)->get('projects')->row()->cost;
    }
    //add an invoice item
    public function add_invoice_item($item)
    {
        return $this->db->insert('invoice_items',$item);
    }
    //create an new invoice
    public function create_invoice($data)
    {
        return $this->db->insert('invoices',$data);
    }
    //Admin: assign project to staff 
    public function assign_project($data)
    {
        return $this->db->where('project_id',$data['project_id'])->update('projects',$data['edit']);
    }
    //Update invoice details
    public function update_invoice($data)
    {
        $amount_due = $this->db->where('ref_no',$data['invoice_ref'])->get('invoices')->row()->amount_due;
        $tax = $this->db->where('ref_no',$data['invoice_ref'])->get('invoices')->row()->tax;
        $new_amount_due = $amount_due + $data['cost'];
        $new_tax_amnt = $tax/100 * $new_amount_due;
        $update = array(
            'amount_due' => $new_amount_due,
            'tax_amnt' => $new_tax_amnt
        );
        return $this->db->where('ref_no',$data['invoice_ref'])->update('invoices',$update);
    }
    //confirm project completion
    public function verify_project($project_id)
    {
        $querry = $this->db->where('project_id',$project_id)->get('projects')->row()->progress;
        if ($querry == 100) {
            return 0;
        } else {
            return 1;
        }
    }
    //returns client email
    public function get_client_email($user_id)
    {
        return $this->db->where('user_id',$user_id)->get('auth')->row()->email;
    }
    //returns company email
    public function get_company_email()
    {
        return $this->db->where('role','admin')->get('auth')->row()->email;
    }
    //returns company name
    public function get_company_name()
    {
        return $this->db->where('role','admin')->get('auth')->row()->company;
    }
    //Add payment
    public function pay_invoice($payment)
    {
        $this->db->insert('payments',$payment['payments']);
        $amnt_paid = $this->db->where('ref_no',$payment['ref_no'])->get('invoices')->row()->amount_paid;
        $new_amnt_paid = $amnt_paid + $payment['invoice'];
        $paid = array('amount_paid' => $new_amnt_paid);
        return $this->db->where('ref_no',$payment['ref_no'])->update('invoices',$paid);
    }
    //returns invoice row()
    public function invoice_info($ref_no)
    {
        return $this->db->where('ref_no',$ref_no)->get('invoices')->row();
    }
    //mark invoice as fully paid
    public function paid($paid)
    {
        return $this->db->where('ref_no',$paid['ref_no'])->update('invoices',$paid['paid']);
    }
    //returns a client's invoices
    public function get_client_invoices($get)
    {
        if ($get['category'] == 'pending') {
            return $this->db->select('*')->from('invoices')
                ->group_start()
                    ->where('client',$get['user_id'])
                    ->where('status','pending')
                ->group_end()
            ->get()->result();
        } else {
            return $this->db->select('*')->from('invoices')
                ->group_start()
                    ->where('client',$get['user_id'])
                    ->where('status','paid')
                ->group_end()
            ->get()->result();
        }
    }
}
