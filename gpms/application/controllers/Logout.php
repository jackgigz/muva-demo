<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //load helper url
        $this->load->helper('url');
    }
    //destroy user session and redirect to login page
    public function index()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}
