<?php

defined('BASEPATH') OR exit('no direct script accessn allowed!');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        if ($this->session->userdata('user_id') != '') 	{
            redirect('home');
        }
    }
    //load login page
    public function index()
    {
        $this->load->view('login.html');
    }
    //load registration page
    public function register()
    {
        $this->load->view('register.html');
    }
}
