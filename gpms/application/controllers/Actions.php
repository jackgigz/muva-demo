<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Actions extends CI_Controller
{
	
    public function __construct()
    {
        //extent prent controler
        parent::__construct();
        //load helper ->url, form, email, date and array
        $this->load->helper(array('url','form','email','date','array'));
        //load library -> email
        $this->load->library('email');
        //load odel -> app_model
        $this->load->model('app_model');
    }
    //change user profile picture
    public function profile_pic()
    {
        $config['upload_path'] = './assets/files/propics/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000kb';
        $config['max_width'] = 1000;
        $config['max_height'] = 1000;
        $config['file_name'] = $this->session->userdata('user_id');
        $config['overwrite'] = TRUE;

        $this->load->library('upload',$config);

        if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('upload_error','<div class="alert alert-danger">'.$error.'</div>');
            redirect('home/profile_edit');
        } else {
            $edit = array('propic' => $this->upload->data('file_name'));
            Auth_lib::edit($edit);
            $this->session->set_flashdata('success','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Profile updated successfully!</div>');
            redirect('home/profile_edit');
        }
    }
    //change user password
    public function profile_passwd()
    {
        if (!$this->input->post()) {
            redirect('home/profile_edit');
        } else {
            $c_pass = $this->input->post('c_pass');
            $n_pass = $this->input->post('n_pass');
            $cn_pass = $this->input->post('cn_pass');
            $c_pass1 = $this->db->where('user_id',$this->session->userdata('user_id'))->get('auth')->row()->password;

            if (!password_verify($c_pass,$c_pass1)) {
                $this->session->set_flashdata('password_error','<div class="alert alert-danger">Invalid current password!</div>');
                redirect('home/profile_edit');
            }

            if ($n_pass != $cn_pass) {
                $this->session->set_flashdata('password_error','<div class="alert alert-danger">New password Mismatch!</div>');
                redirect('home/profile_edit');
            } else {
                $password = password_hash($cn_pass,1);
                $data['edit'] = array('password' => $password);
                $data['user_id'] = $this->session->userdata('user_id');
                Auth_lib::edit($data);
                $this->session->set_flashdata('success','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Profile updated successfully!</div>');
                redirect('home/profile_view');
            }
        }
    }
    //edit user details
    public function profile_edit()
    {
        if (!$this->input->post()) {
            redirect('home/profile_edit');
        } else {
            $data['edit'] = array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'country' => $this->input->post('country')
            );

            $data['user_id'] = $this->session->userdata('user_id');
            Auth_lib::edit($data);
            $this->session->set_flashdata('success','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Profile updated successfully!</div>');
            redirect('home/profile_view');
        }
    }
    //Admin edit user profile
    public function user_profile_edit()
    {
        if (!$this->input->post()) {
            redirct('home/edit_user/'.$this->uri->segment(3));
        } else {
            $data['user_id'] = $this->uri->segment(3);
            $data['edit'] = array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'country' => $this->input->post('country')
            );

            $this->Auth_lib->edit($data);

            $this->session->set_flashdata('success','<div class="alert alert-info alert-dismissable text-center"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Profile Updated Successfully!</div>');
            redirect('home/edit_user/'.$this->uri->segment(3));
        }
    }
    //Admin remove user profile picture
    public function user_propic()
    {
        $data['user_id'] = $this->uri->segment(3);
        $data['edit'] = array('propic' => 'def_propic.jpg');
        $this->app_model->edit($data);

        $this->session->set_flashdata('success','<div class="alert alert-info alert-dismissable text-center"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Profile Updated Successfully</div>');
        redirect('home/edit_user/'.$data['user_id']);
    }
    //user send message
    public function send_message()
    {
        if (!$this->input->post()) {
            redirect('home/compose');
        } else {
            $data['user_to'] = $this->input->post('user_to');
            $data['user_from'] = $this->session->userdata('user_id');
            $data['msg'] = $this->input->post('msg');
            $data['time'] = time();
            $this->app_model->send_message($data);
            $this->session->set_flashdata('success','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Message sent successfully!</div>');
            redirect('home/messages');
        }
    }
    //user trash message
    public function trash()
    {
        $msg_id = $this->uri->segment(3);
        if ($msg_id == '') {
            redirect('home/messages');
        } else {
            $this->app_model->trash($msg_id);
            $this->session->set_flashdata('success','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Message trashed successfully!</div>');
            redirect('home/messages');
        }
    }
    //save message as draft
    public function draft()
    {
        $msg_id = $this->uri->segment(3);
        if ($msg_id == '') {
            redirect('home/messages');
        } else {
            $this->app_model->draft($msg_id);
            $this->session->set_flashdata('success','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Message drafted successfully!</div>');
            redirect('home/messages');
        }
    }
    //message reply
    public function reply()
    {
        $user_id = $this->uri->segment(3);
        if ($user_id == '') {
            redirect('home/messages');
        } else {
            redirect('home/compose/'.$user_id);
        }
    }
    //Admin delete user
    public  function delete_user()
    {
        $user_id = $this->uri->segment(3);
        if ($user_id == 1) {
            $this->session->set_flashdata('delete','<div class="alert alert-danger">Cannot delete Admin!</div>');
            redirect('home/system_users');
        } else {
            $this->app_model->delete_user($user_id);
            $this->session->set_flashdata('delete','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>User deleted!</div>');
            redirect('home/system_users');
        }
    }
    //Admin confirm user
    public function confirm_user()
    {
        $user_id = $this->uri->segment(3);
        if ($user_id == '') {
            redirect('home/system_users');
        } else {
            $data['user_id'] = $user_id;
            $data['edit'] = array('role' => 'staff','stat_conf' => 1);
            Auth_lib::edit($data);
            $this->session->set_flashdata('confirm','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>User Confirmed!</div>');
            redirect('home/system_users');
        }
    }
    //Admin decline user
    public function decline_user()
    {
        $user_id = $this->uri->segment(3);
        if ($user_id == '') {
            redirect('home/system_users');
        } else {
            $data['user_id'] = $user_id;
            $data['edit'] = array('role' => 'client','stat_conf' => 1);
            $this->Auth_lib->edit($data);
            $this->session->set_flashdata('confirm','<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>User Confirmed!</div>');
            redirect('home/system_users');
        }
    }
    //Create new project
    public function new_project()
    {
        if (!$this->input->post()) {
            redirect('home/new_project');
        } else {
            $data = array(
                'project_title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'client' => $this->input->post('client'),
                'manager' => $this->input->post('manager'),
                'start_date' => nice_date($this->input->post('start_date'),'Y-m-d'),
                'due_date' => nice_date($this->input->post('due_date'),'Y-m-d'),
                'cost' => $this->input->post('cost'),
                'currency' => $this->input->post('currency')
            );
            $this->app_model->new_project($data);
            redirect('home/projects');
        }
    }
    //Project conversation
    public function project_chat()
    {
        $project_id = $this->input->post('project_id');
        $data = array(
            'project_id' => $project_id,
            'user_id' => $this->session->userdata('user_id'),
            'message' => $this->input->post('chat'),
            'time' => time(),
            'role' => $this->session->userdata('role')
        );
        $this->app_model->post_project_chat($data);
        redirect('home/view_project/'.$project_id);
    }
    //Update project progress
    public function update_progress()
    {
        if ($this->input->post('progress') == 100) {
            $data['edit'] = array(
                'progress' => $this->input->post('progress'),
                'status' => 'closed',
                'complete_time' => mdate('%Y-%m-%d', time())
            );
            $data['project_id'] = $this->input->post('project_id');
            $this->app_model->edit_project($data);
            redirect('home/projects/closed');
        } else {
            $data['edit'] = array('progress' => $this->input->post('progress'));
            $data['project_id'] = $this->input->post('project_id');
            $this->app_model->edit_project($data);
            redirect('home/view_project/'.$this->input->post('project_id'));
        }
    }
    //Edit project details
    public function edit_project()
    {
        if (!$this->input->post())  {
            redirect('home/projects');
        } else {
            $data['edit'] = array(
                'project_title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'start_date' => nice_date($this->input->post('start_date'),'Y-m-d'),
                'due_date' => nice_date($this->input->post('due_date'),'Y-m-d'),
                'cost' => $this->input->post('cost'),
                'currency' => $this->input->post('currency')
            );
            $data['project_id'] = $this->input->post('project_id');
            $this->app_model->edit_project($data);
            $this->session->set_flashdata('msg','<div class="alert alert-info alert-dismissable"><button class="close" data-dismiss="alert">x</button>Project updated successfully!</div>');
            redirect('home/projects');
        }
    }
    //Admin delete invoice
    public function delete_invoice()
    {
        $invoice_ref = $this->uri->segment(3);
        if ($invoice_ref == '') {
            redirect('home/invoices');
        } else {
            $this->app_model->delete_invoice($invoice_ref);
            $this->session->set_flashdata('msg','<div class="alert alert-info alert-dismissable"><button data-dismiss="alert">x</button>Invoice deleted!</div>');
            redirect('home/invoices');
        }
    }
    //Create new invoice
    public function new_invoice()
    {
        if (!$this->input->post()) {
            redirect('home/create_invoice');
        } else {
            $project_id = $this->input->post('project_id');
            $item = array(
                'project_id' => $project_id,
                'cost' => $this->app_model->get_cost($project_id),
                'invoice_ref' => $this->input->post('ref_no')
            );
            $this->app_model->add_invoice_item($item);

            $data = array(
                'ref_no' => $this->input->post('ref_no'),
                'client' => $this->input->post('client'),
                'date_issued' => $this->input->post('issue_date'),
                'ref_no' => $this->input->post('ref_no'),
                'due_date' => $this->input->post('due_date'),
                'amount_due' => $this->app_model->get_cost($project_id),
                'tax' => $this->input->post('tax'),
                'tax_amnt' => $this->input->post('tax')/100 * $this->app_model->get_cost($project_id),
                'currency' => $this->input->post('currency')
            );
            $this->app_model->create_invoice($data);
            $edit = array('invoiced' => 1);
            $this->app_model->edit_project($edit);
            $this->session->set_flashdata('msg','<div class="alert alert-info alert-dismissable"><button class="close" data-dismiss="alert">x</button>Invoice created Successfully!</div>');
            redirect('home/invoices');
        }
    }
    //Admin assign project to user
    public function assign_project()
    {
        $manager = $this->input->post('manager');
        if (!$manager) {
            redirect('home/assign_project/'.$this->input->post('project_id'));
        } else {
            $data['edit'] = array(
                'manager' => $manager,
                'assigned' => 1
            );
            $data['project_id'] = $this->input->post('project_id');
            $this->app_model->assign_project($data);
            $this->session->set_flashdata('msg','<div class="alert alert-info alert-dismissable"><button class="close" data-dismiss="alert">x</button>Project assigned to <b>'.Auth_lib::get_username($manager).'</b></div>');
            redirect('home/projects');
        }
    }
    //Start project
    public function start_project()
    {
        if (!$this->input->post()) {
            redirect('home/projects');
        } else {
            $data['edit'] = array(
                'start_date' => $this->input->post('start_date'),
                'due_date' => $this->input->post('due_date'),
                'status' => 'open',
                'cost' => $this->input->post('cost'),
                'currency' => $this->input->post('currency')
            );
            $data['project_id'] = $this->input->post('project_id');
            $this->app_model->edit_project($data);
            $this->session->set_flashdata('msg','<div class="alert alert-info alert-dismissable"><button class="close" data-dismiss="alert">x</button>Project updated successfully!</div>');
            redirect('home/view_project/'.$data['project_id']);
        }
    }
    //Admin add invoice Item
    public function add_invoice_item()
    {
        $invoice_ref = $this->input->post('ref_no');
        if (!$this->input->post('project')) {
            redirect('home/view_invoice/'.$invoice_ref);
        }
        $item = array(
            'project_id' => $this->input->post('project'),
            'cost' => $this->app_model->get_cost($this->input->post('project')),
            'invoice_ref' => $invoice_ref
        );
        $this->app_model->add_invoice_item($item);
        $data = array(
            'cost' => $this->app_model->get_cost($this->input->post('project')),
            'invoice_ref' => $invoice_ref
        );
        $this->app_model->update_invoice($data);
        $this->session->set_flashdata('msg','<div class="alert alert-info alert-dismissable"><button class="close" data-dismiss="alert">x</button>Invoice updated successfully!</div>');
        redirect('home/view_invoice/'.$invoice_ref);
    }
    //email invoice as PDF to client
    public function email_invoice()
    {
        $ref_no = $this->uri->segment(3);
        $this->data['invoice_info'] = $this->app_model->get_invoice_info($ref_no);
        $this->data['invoice_items'] = $this->app_model->get_invoice_items($ref_no);
        $this->load->helper (array('dompdf', 'file' ));
        $html = $this->load->view('admin/invoice_pdf',$this->data,TRUE);
        $invoice_ref = $this->uri->segment(3);
        $inv = pdf_create( $html , '',false);
        if ( ! write_file('./assets/files/invoices/invoice'.$this->uri->segment(4).'.pdf', $inv)) {
            echo 'Unable to write the file';
        } else {
            $client_email = $this->app_model->get_client_email($this->uri->segment(4));
            $company_email = $this->app_model->get_company_email();
            $company_name = $this->app_model->get_company_name();
            $this->load->library('email');
            $this->email->from($company_email, $company_name);
            $this->email->to($client_email);
            $this->email->subject('New Invoice');
            $this->email->message('You have received a new Invoice from '.$company_name);
            $this->email->attach('./assets/files/invoices/'.$this->uri->segment(3).'.pdf');

            if (!$this->email->send()) {
                echo "Oops! Something went wrong. Please try again.";
            } else {
                echo "Invoice Sent Successfully"; 
            }
        }
    }
    public function pay_invoice()
    {
        $invoice_ref = $this->input->post('ref_no');
        if (!$this->input->post('payment')) {
            redirect('home/view_invoice/'.$invoice_ref);
        } else {
            $invoice = $this->app_model->invoice_info($invoice_ref);
            $payment['payments'] = array(
                'client_id' => $invoice->client,
                'amount' => $this->input->post('payment'),
                'currency' => $invoice->currency,
                'date' => nice_date(mdate('%d/%m/%Y',time()),'Y-m-d'),
                'invoice_ref' => $invoice_ref);
            $payment['invoice'] = $this->input->post('payment');
            $payment['ref_no'] = $invoice_ref;
            $due_amnt = $invoice->amount_due - $invoice->amount_paid;
            if ($this->input->post('payment') > $due_amnt) {
                $this->session->set_flashdata('msg','<div class="alert alert-info alert-dismissable"><button class="close" data-dismiss="alert">x</button>Invoice is overpaid!</div>');
                redirect('home/view_invoice/'.$invoice_ref);
            } elseif($this->input->post('payment') == $due_amnt) {
                $paid['ref_no'] = $invoice_ref;
                $paid['$paid'] = array('status' => 'paid');
                $this->app_model->paid($paid);
                $this->app_model->pay_invoice($payment);
                $this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable"><button class="close" data-dismiss="alert">x</button>Invoice fully paid.</div>');
                redirect('home/view_invoice/'.$invoice_ref);
            } else {
                $this->app_model->pay_invoice($payment);
                $this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable"><button class="close" data-dismiss="alert">x</button>Payment Successful.</div>');
                redirect('home/view_invoice/'.$invoice_ref);
            }
        }
    }
}