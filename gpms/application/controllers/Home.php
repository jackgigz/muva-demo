<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form','date','array'));
        //load helpers->url,form,date and array
        $this->load->model('app_model');
        //load model->app_model
        $this->role = $this->session->userdata('role');
         //check if an active session exists and redirect to login if no found
        if ($this->session->userdata('user_id') == '') {
            redirect('logout');
        }
    }
    //load the dashboard
    public function index()
    {
        //check session role :if admin, load all projects
        if ($this->role == 'admin') {
            //page title
            $data['title'] = 'Galleryer|Dashboard';
            //all open projects
            $data['o_projects'] = $this->app_model->get_projects('open');
            //all closed projects
            $data['c_projects'] = $this->app_model->get_invoicing_projects();
            //all new projects
            $data['n_projects'] = $this->app_model->get_projects('new');
            //load page header
            $this->load->view('common/header',$data);
            //load page content
            $this->load->view($this->role.'/db');
            //load page footer
            $this->load->view('common/footer');
        } else { //if not admin, load user projects
            //page title
            $data['title'] = 'Galleryer|Dashboard';
            //open user projects
            $data['o_projects'] = $this->app_model->get_user_projects('open');
            //closed user projects
            $data['c_projects'] = $this->app_model->get_user_projects('closed');
            //new user projects
            $data['n_projects'] = $this->app_model->get_user_projects('new');
            //load page header
            $this->load->view('common/header',$data);
            //load page content
            $this->load->view($this->role.'/db');
            //load page footer
            $this->load->view('common/footer');
        }
    }
    //user profile
    public function profile_view()
    {
        //page title
        $data['title'] = 'Galleryer | '.Auth_lib::get_username($this->session->userdata('user_id'));
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/profile');
        //page footer
        $this->load->view('common/footer');
    }
    //user profile edit
    public function profile_edit()
    {
        //page title
        $data['title'] = 'Galleryer | '.Auth_lib::get_username($this->session->userdata('user_id'));
        //load countries
        $data['countries'] = $this->app_model->get_countries();
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/profile_edit');
        //page footer
        $this->load->view('common/footer');
    }
    //messages
    public function messages()
    {
        //message category(inbox, outbox or draft) passed as 3rd segment on uri
        $message_category = $this->uri->segment(3,'inbox');
        //get messages of specified category 
        $data['messages'] = $this->app_model->fetch_messages($message_category);
        //page title
        $data['title'] = 'Galleryer | Messages';
        //page name = message category
        $data['page'] = ucfirst($message_category);
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/messages');
        //page footer
        $this->load->view('common/footer');
    }
    //compose message
    public function compose()
    {
        //third parameter on uri specifies user_to incase its a reply and not a new message
        if ($this->uri->segment(3) == '') {
            //page title
            $data['title'] = 'Galleryer | Messages';
            //get users
            $data['users'] = $this->app_model->get_system_users();
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/compose');
            //page footer
            $this->load->view('common/footer');
        } else {
            //if third parameter is specified i.e user wants to reply to a message sent to them
            $user_id = $this->uri->segment(3); //user_to i.e sender of the message the user is replying
            //page title
            $data['title'] = 'Galleryer | Messages';
            //user_to
            $data['users'] = $this->app_model->get_user($user_id);
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/compose');
            //page footer
            $this->load->view('common/footer');
        }
    }
    //read message
    public function read()
    {
        //third uri parameter = id of message user wants to read
        $msg_id = $this->uri->segment(3);
        if ($msg_id == '') {
            //if $msg_id is NULL, redirect to messages
            redirect('home/messages');
        }
        //mark message as read
        Msg_lib::read($msg_id);
        //page title
        $data['title'] = 'Galleryer | Messages';
        //get message info
        $data['mail'] = $this->app_model->get_message($msg_id);
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/read');
        //page footer
        $this->load->view('common/footer');
    }
    //system users
    public function system_users()
    {
        //redirect to dashboard if user role nid not admin
        if ($this->role != 'admin') {
            redirect('');
        } else {
            //page title
            $data['title'] = 'Galleryer | System Users';
            //get users
            $data['users'] = $this->app_model->get_system_users();
            //get new users
            $data['new_users'] = $this->app_model->get_new_users();
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/system_users');
            //page footer
            $this->load->view('common/footer');
        }
    }
    //create a new project
    public function new_project()
    {
        //page title
        $data['title'] = 'Galleryer | New project';
        //get clients
        $data['clients'] = $this->app_model->get_users('client');
        //get managers
        $data['managers'] = $this->app_model->get_users('staff');
        //get currencies
        $data['currency'] = $this->app_model->currency();
        //get open projects
        $data['open_projects'] = $this->app_model->get_user_projects('open');
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/new_project');
        //page footer
        $this->load->view('common/footer');
    }
    //start project
    public function start_project()
    {
        //project id specified as third parrameter on uri
        $project_id =$this->uri->segment(3);
        if ($project_id == '') {
            //if project id is NULL redirect to projects
            redirect('home/projects');
        } else {
            //page title
            $data['title'] = 'Galleryer | Start project';
            //get project details
            $data['project_info'] = $this->app_model->project_info($project_id);
            //get currencies
            $data['currency'] = $this->app_model->currency();
            //get open projects
            $data['open_projects'] = $this->app_model->get_user_projects('open');
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/start_project');
            //page footer
            $this->load->view('common/footer');
        }
    }
    //show all projects
    public function all_projects()
    {
        //get project category: specifiec as third parameter on uri
        $project_category = $this->uri->segment(3,'open');
        //page title
        $data['title'] = 'Galleryer | Project';
        //get new projects
        $data['show_new_projects'] = $this->app_model->get_projects('new');
        //get projects of specified category
        $data['show_projects'] = $this->app_model->get_projects($project_category);
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/projects');
        //page footer
        $this->load->view('common/footer');
    }
    //user projects
    public function projects()
    {
        //get project category: specifiec as third parameter on uri
        $project_category = $this->uri->segment(3,'open');
        //page title
        $data['title'] = 'Galleryer | Project';
        //get projects of specified category
        $data['show_projects'] = $this->app_model->get_user_projects($project_category);
        $data['show_new_projects'] = $this->app_model->get_user_projects('new');
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/projects');
        //page footer
        $this->load->view('common/footer');
    }
    //admin asign project to staff
    public function assign_project()
    {
        //get project_id
        $project_id = $this->uri->segment(3);
        if ($project_id == '') {
            //if project id is not specified, redirect to projects
            redirect('home/projects');
        } else {
            //page title
            $data['title'] = 'Galleryer | Project';
            //get project details
            $data['project_info'] = $this->app_model->project_info($project_id);
            //get managers *users with role staff or admin
            $data['managers'] = $this->app_model->get_users('staff');
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/assign_project');
            //page footer
            $this->load->view('common/footer');
        }
    }
    //view project
    public function view_project()
    {
        //get project_id
        $project_id = $this->uri->segment(3);
        //check project completion
        $ver = $this->app_model->verify_project($project_id);
        if ($ver == 0) {
            //if project is completed redirect projects
            redirect('home/projects');
        }
        if ($project_id == '') {
	        //if project_id is NULL redirect to prijects
            redirect('home/projects');
        } else {
            //page title
            $data['title'] = 'Galleryer | projects';
            //get project deatils
            $data['project_info'] = $this->app_model->project_info($project_id);
            //get open projects
            $data['open_projects'] = $this->app_model->get_user_projects('open');
            //project conversation
            $get = array('project_id' => $this->uri->segment(3),'show' => $this->uri->segment(4,0));
            //get project conversation
            $data['conversation'] = $this->app_model->get_conversation($get);
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/project_info');
            //page footer
            $this->load->view('common/footer');
        }
    }
    //systm invoices
    public function invoices()
    {
        //if user is the admin load all invoices
        if ($this->role == 'admin') {
            //get invoice category(pending or paid)
            $category = $this->uri->segment(3,'pending');
            //page title
            $data['title'] = 'Galleryer | Invoices';
            //get invoices
            $data['invoices'] = $this->app_model->get_invoices($category);
            //page header
            $this->load->view('common/header',$data);
            //page footer
            $this->load->view($this->role.'/invoices');
            //page content
            $this->load->view('common/footer');
        } else {    //if user is client, load their invoices only
            //page title
            $data['title'] = 'Galleryer | Invoices';
            //invoice category(pending or paid) and user_id
            $get['category'] = $this->uri->segment(3,'pending');
            $get['user_id'] = $this->session->userdata('user_id');
            //get invoices
            $data['invoices'] = $this->app_model->get_client_invoices($get);
            //page header
            $this->load->view('common/header',$data);
            //page footer
            $this->load->view($this->role.'/invoices');
            //page content
            $this->load->view('common/footer');
        }
    }
    //show payments
    public function payments()
    {
        //page title
        $data['title'] = 'Galleryer | Payments project';
        //get payments
        $data['payments'] = $this->app_model->get_payments();
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/payments');
        //page footer
        $this->load->view('common/footer');
    }
    //Admin view user
    public function user_info()
    {
        //page title
        $data['title'] = 'Gallerer | Users';
        //get user details 
        $data['user'] = $this->app_model->user_info($this->uri->segment(3,$this->session->userdata('user_id')));
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/user_info');
        //page footer
        $this->load->view('common/footer');
    }
    //Admin edit user
    public function edit_user()
    {
        //page title
        $data['title'] = 'Gallerer | Users';
        //get user_id
        $data['user_id'] = $this->uri->segment(3);
        //page header
        $this->load->view('common/header',$data);
        //page content
        $this->load->view($this->role.'/edit_user');
        //page footer 
        $this->load->view('common/footer');
    }
    //view invoice
    public function view_invoice()
    {
        //get invoice ref_no.
        $ref_no = $this->uri->segment(3);
        if ($ref_no =='') {
            //if invoice ref_no. id NULL, redirect to invoices
            redirect('home/invoices');
		} else {
            //page title
            $data['title'] = 'Galleryer | Invoices';
            //get invoice details
            $data['invoice_info'] = $this->app_model->get_invoice_info($ref_no);
            //get invoice items
            $data['invoice_items'] = $this->app_model->get_invoice_items($ref_no);
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/view_invoice');
            //page footer
            $this->load->view('common/footer');
        }
    }
    //create invoice
    public function create_invoice()
    {
        //get project id of project being invoiced
        $project_id = $this->uri->segment(3);
        //if project_id is NULL load all projects
        if ($project_id == '') {
            //page title
            $data['title'] = 'Galleryer | Invoices';
            //get clients
            $data['clients'] = $this->app_model->get_users('client');
            $data['selected'] = '';
            //get projects
            $data['project_items'] = $this->app_model->get_projects('closed');
            //get currencies
            $data['currency'] = $this->app_model->currency();
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/new_invoice');
            //page footer
            $this->load->view('common/footer');
        } else { 
            //if project id is spacified
            //get project details
            $project_info = $this->app_model->project_info($project_id);
            //page title
            $data['title'] = 'Galleryer | Invoices';
            //get project client
            foreach ($project_info as $key => $project) {
                $data['clients'] = $this->app_model->get_user($project->client);
            }
            $data['selected'] = 'selected';
            //get projects
            $data['project_items'] = $project_info;
            //get currencies
            $data['currency'] = $this->app_model->currency();
            //page header
            $this->load->view('common/header',$data);
            //page content
            $this->load->view($this->role.'/new_invoice');
            //page footer
            $this->load->view('common/footer');
        }
    }
    //generate invoice PDF
    public function generate_invoice_pdf()
    {
        $ref_no = $this->uri->segment(3);
        $this->data['invoice_info'] = $this->app_model->get_invoice_info($ref_no);
        $this->data['invoice_items'] = $this->app_model->get_invoice_items($ref_no);
        $this->load->helper (array('dompdf', 'file' ));
        $html = $this->load->view($this->role.'/invoice_pdf',$this->data,TRUE);
        pdf_create( $html , 'Invoice '.$ref_no );
    }
}