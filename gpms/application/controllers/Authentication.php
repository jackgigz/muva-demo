<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form','email'));
        $this->load->library('email');
    }
    //Authentify user and create user session
    public function index()
    {
        if (!$this->input->post()) {
            redirect('login');
        } else {
            $user['username'] = $this->input->post('username');
            $user['pwd'] = $this->input->post('password');
            $auth = Auth_lib::Authenticate($user);
            if ($auth == '') {
                $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Invalid Username</div>');
                redirect('login');
            }
            if (password_verify($user['pwd'],$auth)) {
                $session_data = Auth_lib::sess_create($user);
                $this->session->set_userdata($session_data);
                redirect('home');
            } else {
                $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Invalid Password</div>');
                redirect('login');
            }
        }
    }
    //register new user
    public function register()
    {
        if (!$this->input->post()) {
            redirect('login/register');
        }
        if ($this->input->post('password') != $this->input->post('rtpassword')) {
            $this->session->set_flashdata('msg','<div class="alert alert-danger">Password Mismatch!</div>');
            redirect('login/register');
        }
        if (!valid_email($this->input->post('email')))  {
            $this->session->set_flashdata('msg','<div class="alert alert-info">The email you entered is not valid!</div>');
            redirect('login/register');
        }
        $auth_username = Auth_lib::check_username($this->input->post('username'));
        if ($auth_username > 0) {
            $this->session->set_flashdata('msg','<div class="alert alert-info">The username you entered is already taken please pick another one and try again!</div>');
            redirect('login/register');
        }
        if ($this->input->post('stat') == 'staff') {
            $reg['fname'] = $this->input->post('fname');
            $reg['lname'] = $this->input->post('lname');
            $reg['username'] = $this->input->post('username');
            $reg['email'] = $this->input->post('email');
            $reg['password'] = password_hash($this->input->post('password'),1);
            $reg['stat'] = $this->input->post('stat');
            Auth_lib::register($reg);
            $this->session->set_flashdata('success','<div class="alert alert-info">Account created successfully!Please log in.</div>');
            redirect('login');
        } else {
            $reg['fname'] = $this->input->post('fname');
            $reg['lname'] = $this->input->post('lname');
            $reg['username'] = $this->input->post('username');
            $reg['email'] = $this->input->post('email');
            $reg['password'] = password_hash($this->input->post('password'),1);
            $reg['stat'] = $this->input->post('stat');
            $reg['stat_conf'] = 1;
            Auth_lib::register($reg);
            $this->session->set_flashdata('success','<div class="alert alert-info">Account created successfully!Please log in.</div>');
            redirect('login');
        }
    }
}
