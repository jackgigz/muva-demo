<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; <?=date('Y')?> <?=config_item('company_name')?> Powered by <a href="http://codecanyon.net">Galleryer</a>.</strong>
      </footer>
      <script type="text/javascript">
    function confirmdelete(){
        var agree = confirm("Are you sure you want to delete?");
        if(agree){
        return true;
        }else{
        return false;
        }
    }
    function confirmuser(){
        var agree = confirm("This user's status will change to staff. /n Continue?");
        if(agree){
        return true;
        }else{
        return false;
        }
    }
    function declineuser(){
        var agree = confirm("User ststus will remain as client permanently. /n Continue?");
        if(agree){
        return true;
        }else{
        return false;
        }
    }
  </script>
    </div><!-- ./wrapper -->
    <!-- jQuery 2.1.3 -->
    <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- jQuery UI 1.11.2 -->
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- Sparkline -->
    <script src="<?=base_url()?>assets/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=base_url()?>assets/plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="<?=base_url()?>assets/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="<?=base_url()?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?=base_url()?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="<?=base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?=base_url()?>assets/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="<?=base_url();?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?=base_url();?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?=base_url()?>assets/dist/js/pages/dashboard.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>assets/dist/js/demo.js" type="text/javascript"></script>
    <!--page script-->
    <script type="text/javascript">
    // data tables
      $(function () {
        $("#users_table").dataTable();
      });
      $(function () {
        $("#new_users_table").dataTable();
      });
    </script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  </body>
</html>