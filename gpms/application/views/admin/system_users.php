
<body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo"><b>Galleryer</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success"><?=Msg_lib::msgs($this->session->userdata('user_id'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?=Msg_lib::msgs($this->session->userdata('user_id'));?> messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <?php
                    $message = Msg_lib::fetch_msg($this->session->userdata('user_id')); 
                    foreach ($message as $key => $msg) { ?>
                      <li><!-- start message -->
                        <a href="<?=base_url();?>index.php/home/read/<?=$msg->message_id;?>">
                          <div class="pull-left">
                            <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($msg->user_from);?>" class="img-circle" alt="User Image"/>
                          </div>
                          <h4>
                            <?=Auth_lib::get_username($msg->user_from);?>
                            <small><i class="fa fa-clock-o"></i> <?=timespan($msg->time, time(), 1).' ago';?></small>
                          </h4>
                          <p><?=$msg->msg;?></p>
                        </a>
                      </li><!-- end message -->
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?=base_url();?>index.php/home/messages">See All Messages</a></li>
                </ul>
              </li>
              <!-- Projects: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger"><?=Not_lib::get_projects($this->session->userdata('user_id'))?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?=Not_lib::get_projects($this->session->userdata('user_id'))?> Projects in progress</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <?php
                    $projects = Not_lib::fetch_projects($this->session->userdata('user_id'));
                    foreach ($projects as $key => $project) { ?>
                      <li><!-- Task item -->
                        <a href="<?=base_url();?>index.php/home/open_projects/<?=$project->project_id?>">
                          <h3>
                            <?=$project->project_title;?>
                            <small class="pull-right"><?=$project->progress;?>%</small>
                          </h3>
                          <div class="progress xs active">
                            <div class="progress-bar progress-bar-aqua progress-bar-stried" style="width: <?=$project->progress;?>%" role="progressbar" aria-valuenow="<?=$project->progress;?>" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only"><?=$project->progress;?>% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="<?=base_url();?>index.php/home/open_projects">View all Projects</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?=Auth_lib::get_username($this->session->userdata('user_id'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="img-circle" alt="User Image" />
                    <p>
                      <?=Auth_lib::get_username($this->session->userdata('user_id'));?>
                      <small><?=$this->session->userdata('role');?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url();?>index.php/home/profile_view" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url();?>index.php/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?=Auth_lib::get_username($this->session->userdata('user_id'));?></p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?=base_url();?>index.php/home">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Profile</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/profile_view"><i class="fa fa-circle-o text-info"></i> View</a></li>
                <li><a href="<?=base_url();?>index.php/home/profile_edit"><i class="fa fa-circle-o text-info"></i> Edit</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-envelope"></i>
                <span>Messages</span>
                <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/messages"><i class="fa fa-circle-o text-info"></i> Inbox</a></li>
                <li><a href="<?=base_url();?>index.php/home/messages/sent"><i class="fa fa-circle-o text-info"></i> Sent</a></li>
                <li><a href="<?=base_url();?>index.php/home/messages/drafts"><i class="fa fa-circle-o text-info"></i> Draft</a></li>
              </ul>
            </li>
            <li class="active">
              <a href="<?=base_url();?>index.php/home/system_users">
                <i class="fa fa-users"></i>
                <span>System Users</span>
               </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-tasks"></i>
                <span>Projects</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/new_project"><i class="fa fa-circle-o text-info"></i> Create New</a></li>
                <li><a href="<?=base_url();?>index.php/home/all_projects/open"><i class="fa fa-circle-o text-info"></i> Open</a></li>
                <li><a href="<?=base_url();?>index.php/home/all_projects/closed"><i class="fa fa-circle-o text-info"></i> Closed</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-ul"></i> <span>Invoices</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/invoices/pending"><i class="fa fa-circle-o text-info"></i> Pending</a></li>
                <li><a href="<?=base_url();?>index.php/home/invoices/paid"><i class="fa fa-circle-o text-info"></i> Paid</a></li>
              </ul>
            </li>
            <li>
              <a href="<?=base_url();?>index.php/home/payments">
                <i class="fa fa-money"></i> <span>Payments</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>      

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            System Users
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">System users</a></li>
          </ol>
        </section>
        <!-- Main Content-->
        <section class="content">
          <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">New Users</h3>
                  <div class="pull-right box-tools">
                    <!-- button with a dropdown -->
                    <div class="btn-group">
                      <button class="btn btn-info btn-sm" data-widget="collapse" title="collapse"><i class="fa fa-minus"></i></button>
                      <button class="btn btn-info btn-sm" data-widget="remove" title="close"><i class="fa fa-times"></i></button>
                    </div><!-- /. tools -->
                  </div>
                </div>
                <div class="box-body">
                  <?=$this->session->flashdata('confirm');?>
                  <table id="new_users_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>.</th>
                        <th>Username</th>
                        <th>Full Name</th>
                        <th>Usergroup</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($new_users as $key => $user) { ?>
                      <tr>
                        <td><img src="<?=base_url();?>assets/files/propics/<?=$user->propic;?>" width="40" height="40" class="img-circle" /></td>
                        <td><?=$user->username;?></td>
                        <td><?=ucfirst($user->fname).' '.ucfirst($user->lname);?></td>
                        <td><span class="label label-info"><?=$user->role;?></span></td>
                        <td><?=$user->company;?></td>
                        <td><?=$user->email;?></td>
                        <td>
                          <div class="btn-group"> 
                            <a class="btn btn-default btn-sm" href="<?=base_url();?>index.php/home/user_info/<?=$user->user_id;?>">User info</a>
                            <a class="btn btn-default btn-sm" href="<?=base_url();?>index.php/actions/confirm_user/<?=$user->user_id;?>" onclick="return confirmuser()">Confirm</a>
                            <a class="btn btn-default btn-sm" href="<?=base_url();?>index.php/actions/decline_user/<?=$user->user_id;?>" onclick="return declineuser()">Decline</a>
                          </div>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>.</th>
                        <th>username</th>
                        <th>Full Name</th>
                        <th>Usergroup</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th></th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">System Users</h3>
                </div>
                <div class="box-body">
                <?=$this->session->flashdata('delete');?>
                  <table id="users_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>.</th>
                        <th>Username</th>
                        <th>Full Name</th>
                        <th>Usergroup</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($users as $key => $user) { ?>
                      <tr>
                        <td><img src="<?=base_url();?>assets/files/propics/<?=$user->propic;?>" width="40" height="40" class="img-circle" /></td>
                        <td><?=$user->username;?></td>
                        <td><?=ucfirst($user->fname).' '.ucfirst($user->lname);?></td>
                        <td><span class="label label-info"><?=$user->role;?></span></td>
                        <td><?=$user->company;?></td>
                        <td><?=$user->email;?></td>
                        <td>
                          <div class="btn-group"> 
                            <a class="btn btn-default btn-xs" href="<?=base_url();?>index.php/home/user_info/<?=$user->user_id;?>">User info</a>
                            <a class="btn btn-default btn-xs" href="<?=base_url();?>index.php/home/edit_user/<?=$user->user_id;?>">edit</a>
                            <a class="btn btn-default btn-xs" href="<?=base_url();?>index.php/actions/delete_user/<?=$user->user_id;?>" onclick="return confirmdelete()">delete</a>
                          </div>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>.</th>
                        <th>username</th>
                        <th>Full Name</th>
                        <th>Usergroup</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th></th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </section>
      </div><!-- /.content-wrapper -->
