
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?=Auth_lib::get_username($this->session->userdata('user_id'));?></p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="<?=base_url();?>index.php/Cp_client">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Profile</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/cp_client/profile_view"><i class="fa fa-circle-o"></i> View</a></li>
                <li><a href="<?=base_url();?>index.php/cp_client/profile_edit"><i class="fa fa-circle-o"></i> Edit</a></li>
                <li><a href="<?=base_url();?>index.php/cp_client/profile_settings"><i class="fa fa-circle-o"></i> settings</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-briefcase"></i> <span>Portfolio</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-envelope"></i>
                <span>Messages</span>
               </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-users"></i>
                <span>System Users</span>
               </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-tasks"></i>
                <span>Projects</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>