
<body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo"><b>Galleryer</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success"><?=Msg_lib::msgs($this->session->userdata('user_id'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?=Msg_lib::msgs($this->session->userdata('user_id'));?> messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <?php
                    $message = Msg_lib::fetch_msg($this->session->userdata('user_id')); 
                    foreach ($message as $key => $msg) { ?>
                      <li><!-- start message -->
                        <a href="<?=base_url();?>index.php/home/read/<?=$msg->message_id;?>">
                          <div class="pull-left">
                            <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($msg->user_from);?>" class="img-circle" alt="User Image"/>
                          </div>
                          <h4>
                            <?=Auth_lib::get_username($msg->user_from);?>
                            <small><i class="fa fa-clock-o"></i> <?=timespan($msg->time, time(), 1).' ago';?></small>
                          </h4>
                          <p><?=$msg->msg;?></p>
                        </a>
                      </li><!-- end message -->
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?=base_url();?>index.php/home/messages">See All Messages</a></li>
                </ul>
              </li>
              <!-- Projects: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger"><?=Not_lib::get_projects($this->session->userdata('user_id'))?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?=Not_lib::get_projects($this->session->userdata('user_id'))?> Projects in progress</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <?php
                    $projects = Not_lib::fetch_projects($this->session->userdata('user_id'));
                    foreach ($projects as $key => $project) { ?>
                      <li><!-- Task item -->
                        <a href="<?=base_url();?>index.php/home/open_projects/<?=$project->project_id?>">
                          <h3>
                            <?=$project->project_title;?>
                            <small class="pull-right"><?=$project->progress;?>%</small>
                          </h3>
                          <div class="progress xs active">
                            <div class="progress-bar progress-bar-aqua progress-bar-striped" style="width: <?=$project->progress;?>%" role="progressbar" aria-valuenow="<?=$project->progress;?>" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only"><?=$project->progress;?>% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="<?=base_url();?>index.php/home/open_projects">View all Projects</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?=Auth_lib::get_username($this->session->userdata('user_id'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="img-circle" alt="User Image" />
                    <p>
                      <?=Auth_lib::get_username($this->session->userdata('user_id'));?>
                      <small><?=$this->session->userdata('role');?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url();?>index.php/home/profile_view" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url();?>index.php/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?=Auth_lib::get_username($this->session->userdata('user_id'));?></p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?=base_url();?>index.php/home">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Profile</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/profile_view"><i class="fa fa-circle-o text-info"></i> View</a></li>
                <li><a href="<?=base_url();?>index.php/home/profile_edit"><i class="fa fa-circle-o text-info"></i> Edit</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-envelope"></i>
                <span>Messages</span>
                <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/messages"><i class="fa fa-circle-o text-info"></i> Inbox</a></li>
                <li><a href="<?=base_url();?>index.php/home/messages/sent"><i class="fa fa-circle-o text-info"></i> Sent</a></li>
                <li><a href="<?=base_url();?>index.php/home/messages/drafts"><i class="fa fa-circle-o text-info"></i> Draft</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-tasks"></i>
                <span>Projects</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/new_project"><i class="fa fa-circle-o text-info"></i> Create New</a></li>
                <li><a href="<?=base_url();?>index.php/home/projects/open"><i class="fa fa-circle-o text-info"></i> Open</a></li>
                <li><a href="<?=base_url();?>index.php/home/projects/closed"><i class="fa fa-circle-o text-info"></i> Closed</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>      

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Messages
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Messages</a></li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <a href="<?=base_url();?>index.php/home/compose" class="btn btn-primary btn-block margin-bottom">Compose</a>
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Folders</h3>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li class="<?=($page == 'Inbox') ? 'active': '';?>"><a href="<?=base_url();?>index.php/home/messages/inbox"><i class="fa fa-inbox"></i> Inbox </a></li>
                    <li class="<?=($page == 'Sent') ? 'active': '';?>"><a href="<?=base_url();?>index.php/home/messages/sent"><i class="fa fa-envelope-o"></i> Sent</a></li>
                    <li class="<?=($page == 'Drafts') ? 'active': '';?>"><a href="<?=base_url();?>index.php/home/messages/drafts"><i class="fa fa-file-text-o"></i> Drafts</a></li>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?=$page?></h3>
                  <div class="box-tools pull-right">
                    <div class="has-feedback">
                      <input type="text" class="form-control input-sm" placeholder="Search Mail"/>
                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    </div>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <?=$this->session->flashdata('success');?>
                  <div class="table-responsive mailbox-messages">
                    <table class="table table-hover table-striped">
                      <thead>
                        <tr>
                          <th width="20%"><?=($page == 'Inbox') ? 'From': 'To';?></th>
                          <th width="50%">message</th>
                          <th width="20%">time</th>
                          <th width="10%">actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php                        
                        foreach ($messages as $key => $message) { 
                         ?>
                        <tr>
                          <td class="mailbox-name">
                            <a href="<?=base_url();?>index.php/home/read/<?=$message->message_id;?>">
                              <?php $user = ($page == 'Inbox') ? $message->user_from : $message->user_to;
                              echo Auth_lib::get_username($user);?>
                            </a>
                          </td>
                          <td class="mailbox-subject"><?=word_limiter($message->msg,5,'...');?></td>
                          <td class="mailbox-date text-muted">
                          <?= timespan($message->time, time(), 1).' ago';?>
                          </td>
                          <td>
                          <small>
                            <a data-toggle="tool-tip" title="read" href="<?=base_url();?>index.php/read/read/<?=$message->message_id;?>" onclick="<?=Msg_lib::read($message->message_id);?>"><span class="fa fa-envelope <?=($message->status == 'read') ? 'text-muted': '';?>"></span></a>
                            &nbsp;
                            <a data-toggle="tool-tip" title="reply" href="<?=base_url();?>index.php/read/reply/<?=$message->user_from;?>"><span class="fa fa-mail-forward text-muted"></span></a>
                            &nbsp;
                            <a data-toggle="tool-tip" title="trash" href="<?=base_url();?>index.php/read/trash/<?=$message->message_id;?>" onclick="return confirmdelete();"><span class="fa fa-trash text-muted"></span></a>
                          </td>
                          </small>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table><!-- /.table -->
                  </div><!-- /.mail-box-messages -->
                </div><!-- /.box-body -->
                <div class="box-footer no-padding">
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
