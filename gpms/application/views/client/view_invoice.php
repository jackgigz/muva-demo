
<body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo"><b>Galleryer</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success"><?=Msg_lib::msgs($this->session->userdata('user_id'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?=Msg_lib::msgs($this->session->userdata('user_id'));?> messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <?php
                    $message = Msg_lib::fetch_msg($this->session->userdata('user_id')); 
                    foreach ($message as $key => $msg) { ?>
                      <li><!-- start message -->
                        <a href="<?=base_url();?>index.php/home/read/<?=$msg->message_id;?>">
                          <div class="pull-left">
                            <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($msg->user_from);?>" class="img-circle" alt="User Image"/>
                          </div>
                          <h4>
                            <?=Auth_lib::get_username($msg->user_from);?>
                            <small><i class="fa fa-clock-o"></i> <?=timespan($msg->time, time(), 1).' ago';?></small>
                          </h4>
                          <p><?=$msg->msg;?></p>
                        </a>
                      </li><!-- end message -->
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?=base_url();?>index.php/home/messages">See All Messages</a></li>
                </ul>
              </li>
              <!-- Projects: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger"><?=Not_lib::get_projects($this->session->userdata('user_id'))?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?=Not_lib::get_projects($this->session->userdata('user_id'))?> Projects in progress</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <?php
                    $projects = Not_lib::fetch_projects($this->session->userdata('user_id'));
                    foreach ($projects as $key => $project) { ?>
                      <li><!-- Task item -->
                        <a href="<?=base_url();?>index.php/home/open_projects/<?=$project->project_id?>">
                          <h3>
                            <?=$project->project_title;?>
                            <small class="pull-right"><?=$project->progress;?>%</small>
                          </h3>
                          <div class="progress xs active">
                            <div class="progress-bar progress-bar-aqua progress-bar-striped" style="width: <?=$project->progress;?>%" role="progressbar" aria-valuenow="<?=$project->progress;?>" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only"><?=$project->progress;?>% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="<?=base_url();?>index.php/home/open_projects">View all Projects</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?=Auth_lib::get_username($this->session->userdata('user_id'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="img-circle" alt="User Image" />
                    <p>
                      <?=Auth_lib::get_username($this->session->userdata('user_id'));?>
                      <small><?=$this->session->userdata('role');?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url();?>index.php/home/profile_view" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url();?>index.php/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?=Auth_lib::get_username($this->session->userdata('user_id'));?></p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?=base_url();?>index.php/home">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Profile</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/profile_view"><i class="fa fa-circle-o text-info"></i> View</a></li>
                <li><a href="<?=base_url();?>index.php/home/profile_edit"><i class="fa fa-circle-o text-info"></i> Edit</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-envelope"></i>
                <span>Messages</span>
                <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/messages"><i class="fa fa-circle-o text-info"></i> Inbox</a></li>
                <li><a href="<?=base_url();?>index.php/home/messages/sent"><i class="fa fa-circle-o text-info"></i> Sent</a></li>
                <li><a href="<?=base_url();?>index.php/home/messages/drafts"><i class="fa fa-circle-o text-info"></i> Draft</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-tasks"></i>
                <span>Projects</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/new_project"><i class="fa fa-circle-o text-info"></i> Create New</a></li>
                <li><a href="<?=base_url();?>index.php/home/all_projects/open"><i class="fa fa-circle-o text-info"></i> Open</a></li>
                <li><a href="<?=base_url();?>index.php/home/all_projects/closed"><i class="fa fa-circle-o text-info"></i> Closed</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-list-ul"></i> <span>Invoices</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/invoices/pending"><i class="fa fa-circle-o text-info"></i> Pending</a></li>
                <li><a href="<?=base_url();?>index.php/home/invoices/paid"><i class="fa fa-circle-o text-info"></i> Paid</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>      

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Invoice
            <small><?=$this->uri->segment(3);?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Invoice</a></li>
            <li class="active">view</li>
          </ol>
        </section>
        <!-- Main content -->
        <?php foreach ($invoice_info as $key => $invoice) { ?>
        <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe"></i> Galleryer, Inc.
                <small class="pull-right">Date: <?=nice_date($invoice->date_issued,'d/m/Y');?></small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              From
              <address>
                <?php $address = Auth_lib::address($this->session->userdata('user_id'));
                foreach ($address as $key => $user) { ?>
                <strong><?=($user->company == '') ? Auth_lib::get_username($user->user_id): $user->company;?></strong><br>
                <?=$user->email;?>
                <?php } ?>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              To
              <address>
                 <?php $address = Auth_lib::address($invoice->client);
                 foreach ($address as $key => $user) { ?>
                <strong><?=($user->company == '') ? Auth_lib::get_username($user->user_id): $user->company;?></strong><br>
                <?=$user->email;?>
                <?php } ?>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <b>Invoice <?=$invoice->ref_no?></b><br/>
              <br/>
              <b>Payment Due:</b> <?=nice_date($invoice->due_date,'d/m/Y');?><br/>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Description</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($invoice_items as $key => $item) { ?>
                  <tr>
                    <td><?=Not_lib::project_title($item->project_id);?></td>
                    <td><?=word_limiter(Not_lib::project_description($item->project_id),5,'...');?></td>
                    <td><?=Not_lib::project_cost($item->project_id).' '.$invoice->currency;?></td>
                  </tr>
                 <?php } ?>
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <div class="col-xs-6">
              <p class="lead">Amount Due <?=nice_date($invoice->due_date,'d/m/Y');?></p>
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th style="width:50%">Subtotal:</th>
                    <td><?=$invoice->amount_due.' '.$invoice->currency;?></td>
                  </tr>
                  <tr>
                    <th>Tax (<?=$invoice->tax;?>%)</th>
                    <td><?=$invoice->tax_amnt.' '.$invoice->currency;?></td>
                  </tr>
                  <tr>
                    <th>Total:</th>
                    <td><?=$invoice->amount_due + $invoice->tax_amnt.' '.$invoice->currency;?></td>
                  </tr>
                  <tr>
                    <th>Amount Paid:</th>
                    <td><?=$invoice->amount_paid;?></td>
                  </tr>
                  <tr>
                    <th>Amount Due:</th>
                    <td><?=$invoice->amount_due - $invoice->amount_paid;?></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
          <?php } ?>
          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <a class="btn btn-primary btn-lg" href="<?=base_url();?>index.php/home/generate_invoice_pdf/<?=$this->uri->segment(3)?>">Generate PDF</a>
            </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>
      </div><!-- /.content-wrapper -->
