
<body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo"><b>Galleryer</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success"><?=Msg_lib::msgs($this->session->userdata('user_id'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?=Msg_lib::msgs($this->session->userdata('user_id'));?> messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <?php
                    $message = Msg_lib::fetch_msg($this->session->userdata('user_id')); 
                    foreach ($message as $key => $msg) { ?>
                      <li><!-- start message -->
                        <a href="<?=base_url();?>index.php/home/read/<?=$msg->message_id;?>">
                          <div class="pull-left">
                            <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($msg->user_from);?>" class="img-circle" alt="User Image"/>
                          </div>
                          <h4>
                            <?=Auth_lib::get_username($msg->user_from);?>
                            <small><i class="fa fa-clock-o"></i> <?= timespan($msg->time, time(), 1).' ago';?></small>
                          </h4>
                          <p><?=$msg->msg;?></p>
                        </a>
                      </li><!-- end message -->
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?=base_url();?>index.php/home/messages">See All Messages</a></li>
                </ul>
              </li>
              <!-- Projects: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger"><?=Not_lib::get_projects($this->session->userdata('user_id'))?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?=Not_lib::get_projects($this->session->userdata('user_id'))?> Projects in progress</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <?php
                    $projects = Not_lib::fetch_projects($this->session->userdata('user_id'));
                    foreach ($projects as $key => $project) { ?>
                      <li><!-- Task item -->
                        <a href="<?=base_url();?>index.php/home/open_projects/<?=$project->project_id?>">
                          <h3>
                            <?=$project->project_title;?>
                            <small class="pull-right"><?=$project->progress;?>%</small>
                          </h3>
                          <div class="progress xs active">
                            <div class="progress-bar progress-bar-aqua progress-bar-striped" style="width: <?=$project->progress;?>%" role="progressbar" aria-valuenow="<?=$project->progress;?>" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only"><?=$project->progress;?>% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="<?=base_url();?>index.php/home/open_projects">View all Projects</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?=Auth_lib::get_username($this->session->userdata('user_id'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="img-circle" alt="User Image" />
                    <p>
                      <?=Auth_lib::get_username($this->session->userdata('user_id'));?>
                      <small><?=$this->session->userdata('role');?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url();?>index.php/home/profile_view" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url();?>index.php/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?=Auth_lib::get_username($this->session->userdata('user_id'));?></p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?=base_url();?>index.php/home">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Profile</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/profile_view"><i class="fa fa-circle-o text-info"></i> View</a></li>
                <li><a href="<?=base_url();?>index.php/home/profile_edit"><i class="fa fa-circle-o text-info"></i> Edit</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-envelope"></i>
                <span>Messages</span>
                <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/messages"><i class="fa fa-circle-o text-info"></i> Inbox</a></li>
                <li><a href="<?=base_url();?>index.php/home/messages/sent"><i class="fa fa-circle-o text-info"></i> Sent</a></li>
                <li><a href="<?=base_url();?>index.php/home/messages/drafts"><i class="fa fa-circle-o text-info"></i> Draft</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-tasks"></i>
                <span>Projects</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/new_project"><i class="fa fa-circle-o text-info"></i> Create New</a></li>
                <li><a href="<?=base_url();?>index.php/home/projects/open"><i class="fa fa-circle-o text-info"></i> Open</a></li>
                <li><a href="<?=base_url();?>index.php/home/projects/closed"><i class="fa fa-circle-o text-info"></i> Closed</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-ul"></i> <span>Invoices</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url();?>index.php/home/invoices/pending"><i class="fa fa-circle-o text-info"></i> Pending</a></li>
                <li><a href="<?=base_url();?>index.php/home/invoices/paid"><i class="fa fa-circle-o text-info"></i> Paid</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>      

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Project
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url();?>index.php/home"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Project</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Open projects</h3>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <?php foreach ($open_projects as $key => $project) { ?>
                    <li class="<?=($this->uri->segment(3) == $project->project_id) ? 'active': '';?>">
                      <a href="<?=base_url();?>index.php/home/view_project/<?=$project->project_id;?>"><span class="fa fa-circle"></span> <?=$project->project_title;?></a>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="box box-info">
                  <div class="box-header with-border">
                    <?php foreach ($project_info as $key => $project) { ?>
                    <h3 class="box-title"><?=$project->project_title;?></h3>
                  </div>
                  <div class="box-body">
                    <div class="row">
                      <div class="col-lg-4 col-md-4">
                        <img data-toggle="tool-tip" title="project thumbnail" src="<?=base_url();?>assets/files/project/project_thumbnails/def_project_thumbnail.jpg" width="100%">
                      </div>
                      <div class="col-lg-8 col-md-4">
                        <table class="table">
                          <tbody>
                            <tr>
                              <th>Title:</th>
                              <td><?=$project->project_title;?></td>
                            </tr>
                            <tr>
                              <th>Description:</th>
                              <td data-toggle="tooltip" title="<?=$project->description;?>"><?=word_limiter($project->description,5,'...');?></td>
                            </tr>
                            <tr>
                              <th>Manager:</th>
                              <td><?=Auth_lib::get_username($project->manager);?></td>
                            </tr>
                            <tr>
                              <th>Due date:</th>
                              <td><?=$project->due_date;?></td>
                            </tr>
                            <tr>
                              <th>Progress</th>
                              <td><?=$project->progress;?> % </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div><!--./col-lg-8 col-md-8-->
                    </div><!--./row-->
                  </div><!--./box-body-->
                  <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-chat"></i> Project conversation</h3>
                  </div>
                  <div class="box-body">
                    <ul class="timeline">

                      <!-- timeline time label -->
                      <li class="time-label">
                        <span class="bg-red">
                        <?php
                          $datestring = '%d %m %Y';
                          $time = time();
                          echo mdate($datestring, $time);
                        ?>
                        </span>
                      </li>
                      <!-- /.timeline-label -->
                      <li>
                        <img style="margin-left:7px;" class="img img-circle" width="50px" src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($this->session->userdata('user_id'));?>">
                        <div class="timeline-item">
                          <h4 class="timeline-header"><?=Auth_lib::get_username($this->session->userdata('user_id'));?></h4>
                          <?php echo form_open('actions/project_chat');?>
                          <fieldset>
                            <input type="hidden" name="project_id" value="<?=$this->uri->segment(3);?>">
                            <div class="input-group">
                              <input type="text" class="form-control" name="chat" required/>
                              <span class="input-group-btn">
                                <input type="submit" value="Post" class="btn btn-primary">
                              </span>
                            </div>
                          </fieldset>
                          <?php echo form_close();?>
                        </div>
                      </li>
                      <?php foreach ($conversation as $key => $chat) { ?>
                      <!-- timeline item -->
                      <li>
                        <!-- timeline icon -->
                        <img style="margin-left:7px;" class="img img-circle" width="50px" src="<?=base_url();?>assets/files/propics/<?=Auth_lib::get_propic($chat->user_id);?>">
                        <div class="timeline-item">
                          <span class="time"><i class="fa fa-clock-o"></i> <?= timespan($chat->time, time(), 1).' ago';?></span>

                          <h4 class="timeline-header <?=($chat->role == 'staff') ? 'text-primary': 'text-warning';?>"><?=Auth_lib::get_username($chat->user_id);?></h4>

                          <div class="timeline-body">
                          <?=$chat->message;?>
                          </div>
                        </div>
                      </li>
                      <!-- END timeline item -->
                      <?php } ?>
                    </ul>
                    <div class="text-center">
                      <a href="<?=base_url();?>index.php/home/view_project/<?=$this->uri->segment(3);?>/all" class="btn btn-success btn-flat">Show all</a>
                    </div>
                  </div>
                </div><!--./box-->
              </div><!--./col-md-9-->
            </div><!--./row-->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
