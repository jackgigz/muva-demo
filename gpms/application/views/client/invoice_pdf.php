<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Invoice</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  </head>
  <body>
      <?php foreach ($invoice_info as $key => $invoice) { ?>
      <h2>Galleryer, Inc.</h2>
       Date: <?=nice_date($invoice->date_issued,'d/m/Y');?></small>
       <table width="100%">
         <thead>
           <tr>
             <th width="50%">From:</th>
             <th width="50%">To:</th>
           </tr>
         </thead>
         <tbody>
           <tr>
            <?php $address = Auth_lib::address($this->session->userdata('user_id'));
            foreach ($address as $key => $user) { ?>
             <td><strong><?=($user->company == '') ? Auth_lib::get_username($user->user_id): $user->company;?></strong><br></td>
             <?php } ?>
             <?php $address = Auth_lib::address($this->session->userdata('user_id'));
            foreach ($address as $key => $user) { ?>
             <td><strong><?=($user->company == '') ? Auth_lib::get_username($user->user_id): $user->company;?></strong><br></td>
             <?php } ?>
           </tr>
           <tr>
            <?php $address = Auth_lib::address($this->session->userdata('user_id'));
            foreach ($address as $key => $user) { ?>
             <td><?=$user->email?></td>
             <?php } ?>
             <?php $address = Auth_lib::address($invoice->client);
            foreach ($address as $key => $user) { ?>
             <td><?=$user->email?></td>
             <?php } ?>
           </tr>
         </tbody>
       </table>
      <h2>Invoice <?=$invoice->ref_no?></h2>
      <b>Payment Due:</b> <?=nice_date($invoice->due_date,'d/m/Y');?><br/>
      <!-- Table row -->
              <table width="100%">
                <thead>
                  <tr>
                    <th width="25%">Product</th>
                    <th width="25%">Description</th>
                    <th width="25%">Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($invoice_items as $key => $item) { ?>
                  <tr>
                    <td><?=Not_lib::project_title($item->project_id);?></td>
                    <td><?=word_limiter(Not_lib::project_description($item->project_id),5,'...');?></td>
                    <td><?=Not_lib::project_cost($item->project_id).' '.$invoice->currency;?></td>
                  </tr>
                 <?php } ?>
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->
          <br/>
          <p class="lead">Amount Due <?=nice_date($invoice->due_date,'d/m/Y');?></p>
          <br/>
            <table width="100%">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td><?=$invoice->amount_due.' '.$invoice->currency;?></td>
              </tr>
              <tr>
                <th>Tax (<?=$invoice->tax;?>%)</th>
                <td><?=$invoice->tax_amnt.' '.$invoice->currency;?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td><?=$invoice->amount_due + $invoice->tax_amnt.' '.$invoice->currency;?></td>
              </tr>
            </table>
          <?php } ?>
  </body>
</html>