<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Msg_lib
{

    private static $db;
    protected $ci;

    // Define system tables

    public static $user_table = 'messages';

    function __construct()
    {
        $this->ci =&get_instance();
        $this->ci->load->database();

        self::$db = &get_instance()->db;
    }
    //returns number of user's new messages
    static function msgs($user_id)
    {
        return self::$db->select('*')->from('messages')
            ->group_start()
                ->where('user_to',$user_id)
                ->where('status','unread')
            ->group_end()
        ->get()->num_rows();
    }
    //returns user's new messages 
    static function fetch_msg($user_id)
    {
        return self::$db->order_by('message_id','DESC')->limit(8)->select('*')->from('messages')
            ->group_start()
                ->where('user_to',$user_id)
                ->where('status','unread')
            ->group_end()
        ->get()->result();
    }
    //mark message as read
    static function read($message_id)
    {
        return self::$db->where('message_id',$message_id)->update('messages',array('status' => 'read'));
    }
}
