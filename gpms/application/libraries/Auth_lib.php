<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_lib
{
    private static $db;
    protected $ci;

    // Define system tables

    public static $user_table = 'auth';

    function __construct()
    {
        $this->ci =&get_instance();
        $this->ci->load->database();

        self::$db = &get_instance()->db;
    }
    //returns user password 
    static function Authenticate($user)
    {
        return self::$db->where('username',$user['username'])->get('auth')->row()->password;
    }
    //returns user session_data
    static  function sess_create($user)
    {
        $auth = self::$db->where('username',$user['username'])->get('auth')->row();
        $session_data = array(
            'user_id'   => $auth->user_id,
            'username'  => $auth->username,
            'role'      => $auth->role,
            'logged_in' => TRUE
            );
        return $session_data;
    }
    //Add new user 
    static function register($reg)
    {
        return self::$db->insert('auth',$reg);
    }
    //returns user's full name
    static function get_username($user_id)
    {
        $fname = self::$db->where('user_id',$user_id)->get('auth')->row()->fname;
        $lname = self::$db->where('user_id',$user_id)->get('auth')->row()->lname;

        return ucfirst($fname).' '.ucfirst($lname);
    }
    //returns user's profile picture filename
    static function get_propic($user_id)
    {
        return self::$db->where('user_id',$user_id)->get('auth')->row()->propic;
    }
    //returns user deatils
    static function profile_info($user_id)
    {
        return self::$db->where('user_id',$user_id)->get('auth')->result();
    }
    //Update user details
    static function edit($data)
    {
        return self::$db->where('user_id',$data['user_id'])->update('auth',$data['edit']);
    }
    //check if a username picked by a new user exists
    static function check_username($username)
    {
        return self::$db->where('username',$username)->get('auth')->num_rows();
    }
    //returns user address information
    static function address($user_id)
    {
        return self::$db->where('user_id',$user_id)->get('auth')->result();
    }
}
