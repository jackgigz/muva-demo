<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Not_lib
{

    private static $db;
    protected $ci;

    // Define system tables

    public static $user_table = 'notifications';

    function __construct()
    {
        $this->ci =&get_instance();
        $this->ci->load->database();

        self::$db = &get_instance()->db;
    }
    //returns new projects
    static function not_new_projects($user_id)
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('manager',$user_id)
                ->where('progress',0)
            ->group_end()
        ->get()->result();
    }
    //returns complete projects
    static function not_complete_projects($user_id)
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('manager',$user_id)
                    ->or_group_start()
                        ->where('client',$user_id)
                    ->group_end()
            ->group_end()
            ->where('status','closed')
            ->where('invoiced',0)
        ->get()->result();
    }
    //returns number of open projects
    static function get_projects($user_id)
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('manager',$user_id)
                    ->or_group_start()
                        ->where('manager',$user_id)
                    ->group_end()
            ->group_end()
            ->where('status','open')
        ->get()->num_rows();
    }
    //returns open projects
    static function fetch_projects($user_id)
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('manager',$user_id)
                ->or_group_start()
                    ->where('manager',$user_id)
                ->group_end()
            ->group_end()
            ->where('status','open')
        ->get()->result();
    }
    //returns number of all complete projects
    static function complete_projects()
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('status','closed')
                ->where('invoiced',0)
            ->group_end()
        ->get()->num_rows();
    }
    //returns number of user complete projects
    static function complete_user_projects($user_id)
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('manager',$user_id)
                ->or_group_start()
                    ->where('client',$user_id)
                ->group_end()
            ->group_end()
            ->where('status','closed')
            ->where('invoiced',0)
        ->get()->num_rows();
    }
    //returns number of new projects
    static function new_projects()
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('progress',0)
                ->where('manager','')
            ->group_end()
        ->get()->num_rows();
    }
    //returns number of new user projects
    static function new_user_projects($user_id)
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('manager',$user_id)
                ->or_group_start()
                    ->where('client',$user_id)
                ->group_end()
            ->group_end()
            ->where('progress',0)
        ->get()->num_rows();
    }
    //returns number of new users
    static function new_users()
    {
        return self::$db->where('stat_conf',0)->get('auth')->num_rows();
    }
    //returns number of pending invoices
    static function pending_invoices()
    {
        return self::$db->where('amount_paid',0)->get('invoices')->num_rows();
    }
    //returns the nummber of a user's pending invoices
    static function user_pending_invoices($user_id)
    {
        return self::$db->select('*')->from('invoices')
            ->group_start()
                ->where('client',$user_id)
                ->where('status','pending')
            ->group_end()
        ->get()->num_rows();
    }
    //returns users in specified user group
    static function users($role)
    {
        return self::$db->where('role',$role)->get('auth')->num_rows();
    }
    //returns nunber user's current projects
    static function current_projects($user_id)
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('manager',$user_id)
                ->or_group_start()
                    ->where('client',$user_id)
                ->group_end()
            ->group_end()
            ->where('status','open')
        ->get()->num_rows();
    }
    //returns number of user's open projects
    static function open_projects($user_id)
    {
        return self::$db->select('*')->from('projects')
            ->group_start()
                ->where('manager',$user_id)
                ->where('status','open')
            ->group_end()
        ->get()->num_rows();
    }
    //returns project title 
    static function project_title($project_id)
    {
        return self::$db->where('project_id',$project_id)->get('projects')->row()->project_title;
    }
    //returns project description
    static function project_description($project_id)
    {
        return self::$db->where('project_id',$project_id)->get('projects')->row()->description;
    }
    //returns project cost
    static function project_cost($project_id)
    {
        return self::$db->where('project_id',$project_id)->get('projects')->row()->cost;
    }
}
